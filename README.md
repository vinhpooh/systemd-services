# Installation

NGROK : 
Place [`ngrok`](https://ngrok.com/download) in `/etc/ngrok/`
Get `authtoken` from ngrok website, then add it to `/etc/ngrok/ngrok.yml`
Add `ngrok.service` to `/etc/systemd/system/`
Start ngrok service by typing:
```
    systemctl daemon-reload
    systemctl enable ngrok.service
    systemctl start ngrok.service
```

PROMETHEUS :
Install prometheus : apt install prometheus
Add `prometheus.service` to `/etc/systemd/system/`
Start prometheus service by typing:
```
    systemctl daemon-reload
    systemctl enable prometheus.service
    systemctl start prometheus.service
```

PROMETHEUS-NODE-EXPORTER :
Install prometheus : apt install prometheus-node-exporter
Add `prometheus-node-exporter.service` to `/etc/systemd/system/`
Start prometheus-node-exporter service by typing:
```
    systemctl daemon-reload
    systemctl enable prometheus-node-exporter.service
    systemctl start prometheus-node-exporter.service
```
